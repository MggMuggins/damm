# `damm`
Implements the [Damm algorithm](https://en.wikipedia.org/wiki/Damm_algorithm). See the [docs](https://docs.rs/damm) for more info.
